import { Component, OnInit } from '@angular/core'
import { images, Image } from '../../../assets/images'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  public innerImages: Image[] = []

  ngOnInit() {
    this.innerImages = images
  }

  selectImage = (url :string) => {
    for (let image of this.innerImages) {
      if (image.url === url) {
        image.selected = !image.selected
      }
    }
    this.innerImages['hide']()
  }
}
