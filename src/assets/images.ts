export interface Image {
  url: string
  selected?: boolean
}

export const images: Image[] = [
  { url: 'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-01.jpg' },
  { url: 'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-02.jpg' },
  { url: 'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-03.jpg' },
  { url: 'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-04.jpg' },
  { url: 'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-05.jpg' },
  { url: 'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-00.jpg' },
  { url: 'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-06.jpg' },
  { url: 'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-08.jpg' },
  { url: 'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-09.jpg' },
  { url: 'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-11.jpg' },
  { url: 'https://google.fr/image.png' }
]
